module.exports = {
  siteMetadata: {
    siteTitle: `@rocketseat/gatsby-theme-docs`,
    defaultTitle: `@rocketseat/gatsby-theme-docs`,
    siteTitleShort: `Macanta Technical Documentation`,
    siteDescription: `Out of the box Gatsby Theme for creating documentation websites easily and quickly`,
    siteUrl: `https://readthedocs.macanta.org`,
    siteAuthor: `Peter Daly-Dickson`,
    siteImage: `/banner.png`,
    siteLanguage: `en`,
    themeColor: `#7159c1`,
    basePath: `/`,
    footer: `Theme by Rocketseat`,
  },
  plugins: [
    {
      resolve: `@rocketseat/gatsby-theme-docs`,
      options: {
        configPath: `src/config`,
        docsPath: `src/docs`,
        gitlabUrl: `https://gitlab.com/macanta/macanta-docs`,
        baseDir: ``,
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Rocketseat Gatsby Themes`,
        short_name: `RS Gatsby Themes`,
        start_url: `/`,
        background_color: `#ffffff`,
        display: `standalone`,
        icon: `static/favicon.png`,
      },
    },
    `gatsby-plugin-sitemap`,
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        // trackingId: ``,
      },
    },
    {
      resolve: `gatsby-plugin-canonical-urls`,
      options: {
        siteUrl: `https://readthedocs.macanta.org`,
      },
    },
    `gatsby-plugin-offline`,
  ],
};
