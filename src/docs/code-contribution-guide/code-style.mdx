---
title: Code Style and Conventions
---

One can summarise Macanta's coding philosophy as a relentless focus on making the
codebase easy to understand, and in which it is difficult to make dangerous mistakes.

The majority of work in any large software development project is understanding the
existing code so it can be debugged or modified. Investments in code readability
usually end up paying for themselves when someone inevitably has to debug or
improve the code.

When there's something subtle or complex to explain or ensure in the implementation,
we try hard to make it clear through a combination of clean and intuitive interfaces,
well-named variables and functions, comments/docstrings, and commit messages, roughly
in that order of priority. If you can make something clear with a good interface,
that's a lot better than writing a comment explaining how the bad interface works.

This page documents code style policies that every Macanta developer should
understand.  We aim for this document to be short and focused only on details that
cannot be easily enforced another way e.g. through linters, automated tests,
subsystem design that makes classes of mistakes unlikely, etc.

This approach minimises the cognitive load of ensuring a consistent coding style for
both contributors and maintainers.

## Be consistent!

Look at the surrounding code, or a similar part of the project, and try
to do the same thing. If you think the other code has actively bad
style, fix it -- in a separate commit.

When in doubt, ask in [macanta.zulipchat.com](https://macanta.zulipchat.com).

## Secrets

Please don't put any passwords, secret access keys, etc. inline in the
code. 

## JS array/object manipulation

For functions that operate on arrays or JavaScript objects, you should
generally use modern
[ECMAScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Language_Resources)
primitives such as [`for … of`
loops](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...of),
[`Array.prototype.{entries, every, filter, find, indexOf, map,
some}`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array),
[`Object.{assign, entries, keys,
values}`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object),
[spread
syntax](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax),
and so on. 

## More arbitrary style things

### Line length

There is an absolute hard limit on line length for only some files, though we should
still avoid extremely long lines. A general guideline is: refactor stuff to get it
under 85 characters, unless that makes the code a lot uglier, in which case it's fine
to go up to 120 or so.

### HTML / CSS

Avoid using the `style=` attribute unless the styling is actually
dynamic. Instead, define logical classes and put your styles in
external CSS files such as `macanta.css`.

Don't use the tag name in a selector unless you have to. In other words,
use `.foo` instead of `span.foo`. We shouldn't have to care if the tag
type changes in the future.

### Tests

Clear, readable code is important for tests;
<!-- familiarize yourself with our testing frameworks so that you can write clean,
readable tests.  -->
Comments about anything subtle about what is being verified are appreciated.
