import React from 'react';
import PropTypes from 'prop-types';
import { MdEdit } from 'react-icons/md';

export default function EditGitlab({ gitlabEditUrl }) {
  if (gitlabEditUrl) {
    return (
      <a
        href={gitlabEditUrl}
        target="_blank"
        rel="noopener noreferrer"
        style={{
          display: 'flex',
          alignItems: 'center',
          textDecoration: 'none',
          marginTop: '48px',
          color: '#78757a',
          opacity: '0.8',
          fontSize: '14px',
          fontWeight: 'normal',
        }}
      >
        <MdEdit style={{ marginRight: '5px' }} />
        Edit this page on GitLab
      </a>
    );
  }
  return null;
}

EditGitlab.propTypes = {
  gitlabEditUrl: PropTypes.string,
};

EditGitlab.defaultProps = {
  gitlabEditUrl: null,
};
